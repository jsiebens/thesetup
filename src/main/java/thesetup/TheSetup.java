/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package thesetup;

import static org.eclipse.jetty.util.resource.Resource.*;

import java.io.File;
import java.io.IOException;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;
import thesetup.model.Workbench;
import thesetup.model.WorkbenchHolder;
import thesetup.rest.IndexHtml;
import thesetup.rest.WorkspaceResouce;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lexicalscope.jewel.cli.ArgumentValidationException;
import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;
import com.sun.jersey.api.core.DefaultResourceConfig;
import com.sun.jersey.spi.container.servlet.ServletContainer;

/**
 * @author Johan Siebens
 */
public class TheSetup {

    public static interface Cli {

        @Option(shortName = { "d" })
        File getDir();

        @Option(shortName = "p", defaultValue = "8080")
        int getPort();

    }

    public static void main(String[] args) throws Exception {
        try {
            Cli cli = CliFactory.parseArguments(Cli.class, args);

            Resource dir = newResource(cli.getDir());

            Resource packages = dir.getResource("packages");
            Resource resources = dir.getResource("resources");

            WorkbenchHolder.set(readConfig(packages));

            Templates templates = new Templates();

            DefaultResourceConfig application = new DefaultResourceConfig();
            application.getSingletons().add(new IndexHtml(templates));
            application.getSingletons().add(new WorkspaceResouce(templates, packages));

            ServletContextHandler servlet = new ServletContextHandler();
            servlet.addServlet(new ServletHolder(new ServletContainer(application)), "/*");

            ContextHandlerCollection handler = new ContextHandlerCollection();
            handler.addHandler(resourceHandler("/assets", newClassPathResource("thesetup/assets")));
            handler.addHandler(resourceHandler("/resources", resources));
            handler.addHandler(servlet);

            Server server = new Server(cli.getPort());
            server.setHandler(handler);
            server.start();
        }
        catch (ArgumentValidationException e) {
            System.out.println(e.getMessage());
        }
    }

    public static Handler resourceHandler(String context, Resource resource) {
        ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setBaseResource(resource);
        ContextHandler handler = new ContextHandler();
        handler.setHandler(resourceHandler);
        handler.setContextPath(context);
        return handler;
    }

    private static Workbench readConfig(Resource packages) throws IOException, JsonParseException, JsonMappingException {
        return new ObjectMapper().readValue(packages.getResource("config.json").getInputStream(), Workbench.class);
    }

}
