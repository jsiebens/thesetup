/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package thesetup.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.CharMatcher;

/**
 * @author Johan Siebens
 */
public class Workspace {

    private static String validateNull(String name, String value) {
        if (value == null) {
            throw new IllegalArgumentException("missing property '" + name + "' is missing");
        }
        return value;
    }

    private static String validatePath(String path) {
        if (CharMatcher.inRange('a', 'z')
                .or(CharMatcher.inRange('0', '9'))
                .or(CharMatcher.is('_'))
                .matchesAllOf(validateNull("path", path))) {
            return path;
        }
        throw new IllegalArgumentException("illegal path, only lowercase a-z, or 0-9, or _ is allowed");
    }

    private final String name;

    private final List<String> packages;

    private final String path;

    private final Map<String, Object> properties;

    @JsonCreator
    public Workspace(
            @JsonProperty("path") String path,
            @JsonProperty("name") String name,
            @JsonProperty("packages") List<String> packages) {
        this.path = validatePath(path);
        this.name = validateNull("name", name);
        this.packages = Collections.unmodifiableList(new ArrayList<String>(packages));
        this.properties = new LinkedHashMap<String, Object>();
    }

    public String getName() {
        return name;
    }

    public List<String> getPackages() {
        return packages;
    }

    public String getPath() {
        return path;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    @JsonAnySetter
    public void set(String key, Object value) {
        this.properties.put(key, value);
    }

}