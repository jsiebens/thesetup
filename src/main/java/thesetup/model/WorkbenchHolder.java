/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package thesetup.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class WorkbenchHolder {

    private static final AtomicReference<Workbench> ref = new AtomicReference<Workbench>();

    public static Workbench get() {
        return ref.get();
    }

    public static void set(Workbench config) {
        ref.set(config);
    }

    public static Map<String, Object> workspaceAsMap(String path) {
        for (Workspace workspace : workspaces()) {
            if (workspace.getPath().equals(path)) {
                Map<String, Object> map = new HashMap<String, Object>(workspace.getProperties());
                map.put("name", workspace.getName());
                map.put("path", workspace.getPath());
                map.put("packages", workspace.getPackages());
                map.put("dir", dir());
                return map;
            }
        }
        return null;
    }

    public static List<Workspace> workspaces() {
        return ref.get().getWorkspaces();
    }

    private static String dir() {
        return ref.get().getDir();
    }

}
