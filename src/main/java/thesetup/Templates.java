/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package thesetup;

import static org.eclipse.jetty.util.resource.Resource.*;

import java.io.InputStreamReader;

import org.eclipse.jetty.util.resource.Resource;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Mustache.Compiler;
import com.samskivert.mustache.Template;

/**
 * @author Johan Siebens
 */
public final class Templates {

    private final Compiler compiler;

    private Template envCmdTemplate;

    private Template indexHtmlTemplate;

    private Template installAllCmdTemplate;

    private Template installCmdTemplate;

    private Template setupCmdTemplate;

    public Templates() throws Exception {
        this.compiler = Mustache.compiler();
        indexHtmlTemplate = get(newClassPathResource("thesetup/index.html.mustache"));
        envCmdTemplate = get(newClassPathResource("thesetup/installer/env.cmd.mustache"));
        installCmdTemplate = get(newClassPathResource("thesetup/installer/install.cmd.mustache"));
        installAllCmdTemplate = get(newClassPathResource("thesetup/installer/install_all.cmd.mustache"));
        setupCmdTemplate = get(newClassPathResource("thesetup/installer/setup.cmd.mustache"));
    }

    public final Template get(Resource resource) throws Exception {
        return compiler.compile(new InputStreamReader(resource.getInputStream()));
    }

    public Template getEnvCmdTemplate() {
        return envCmdTemplate;
    }

    public Template getIndexHtmlTemplate() {
        return indexHtmlTemplate;
    }

    public Template getInstallAllCmdTemplate() {
        return installAllCmdTemplate;
    }

    public Template getInstallCmdTemplate() {
        return installCmdTemplate;
    }

    public Template getSetupCmdTemplate() {
        return setupCmdTemplate;
    }

}
