/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package thesetup.rest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.eclipse.jetty.http.MimeTypes;
import org.eclipse.jetty.util.resource.Resource;
import thesetup.Templates;
import thesetup.model.WorkbenchHolder;

import com.google.common.base.CharMatcher;
import com.samskivert.mustache.Template;
import com.sun.jersey.api.NotFoundException;

/**
 * @author Johan Siebens
 */
@Path("/workspace")
public class WorkspaceResouce {

    private static void writeZipEntry(String name, Resource resource, ZipOutputStream out) throws IOException {
        writeZipEntry(name, out, resource.getInputStream());
    }

    private static void writeZipEntry(String name, Template template, Map<String, Object> data, ZipOutputStream out) throws IOException {
        StringWriter s = new StringWriter();
        template.execute(data, s);
        writeZipEntry(name, out, new ByteArrayInputStream(s.toString().getBytes()));
    }

    private static void writeZipEntry(String name, ZipOutputStream out, InputStream in) throws IOException {
        out.putNextEntry(new ZipEntry(name));
        byte[] b = new byte[1024];
        int count;
        while ((count = in.read(b)) > 0) {
            out.write(b, 0, count);
        }
        out.closeEntry();
    }

    private MimeTypes mimeTypes = new MimeTypes();

    private final Resource packages;

    private Templates templates;

    @Context
    private UriInfo uri;

    public WorkspaceResouce(Templates templates, Resource packages) {
        this.templates = templates;
        this.packages = packages;
        this.mimeTypes.addMimeMapping("cmd", "text/plain");
        this.mimeTypes.addMimeMapping("bat", "text/plain");
        this.mimeTypes.addMimeMapping("zip", "application/zip");
    }

    @GET
    @Path("{name}/setup.cmd")
    public String cmd(@PathParam("name") String name) {
        return templates.getSetupCmdTemplate().execute(getWorkspace(name));
    }

    @GET
    @Path("{name}/packages/{path:.*}")
    public Response packages(@PathParam("name") final String name, @PathParam("path") final String path) throws Exception {
        getWorkspace(name);

        final Callable<Response> notFound = new Callable<Response>() {

            public Response call() throws Exception {
                throw new NotFoundException();
            }

        };

        final Callable<Response> defaultPackage = new Callable<Response>() {

            public Response call() throws Exception {
                return lookupPackageResource(name, "default/" + path, notFound);
            }

        };

        return lookupPackageResource(name, name + '/' + path, defaultPackage);
    }

    @GET
    @Path("{path}/setup.zip")
    public Response zip(@PathParam("path") String path) throws Exception {
        Map<String, Object> data = getWorkspace(path);

        String setupName = "setup/" + path;

        data.put("url", CharMatcher.is('/').trimTrailingFrom(uri.getBaseUri().toString()));

        Resource base = Resource.newClassPathResource("thesetup/installer");

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(out);
        writeZipEntry(setupName + "/bin/unzip.exe", base.getResource("bin/unzip.exe"), zip);
        writeZipEntry(setupName + "/bin/wget.exe", base.getResource("bin/wget.exe"), zip);
        writeZipEntry(setupName + "/bin/xxmklink.exe", base.getResource("bin/xxmklink.exe"), zip);

        writeZipEntry(setupName + "/env.cmd", templates.getEnvCmdTemplate(), data, zip);
        writeZipEntry(setupName + "/install.cmd", templates.getInstallCmdTemplate(), data, zip);
        writeZipEntry(setupName + "/install_all.cmd", templates.getInstallAllCmdTemplate(), data, zip);

        zip.close();

        return Response
                .ok()
                .header("Content-Type", mimeTypes.getMimeByExtension("setup.zip"))
                .entity(out.toByteArray())
                .build();
    }

    private Map<String, Object> getWorkspace(String path) {
        Map<String, Object> map = WorkbenchHolder.workspaceAsMap(path);
        if (map != null) {
            return map;
        }
        throw new NotFoundException();
    }

    private Response lookupPackageResource(String name, String path, Callable<Response> fallback) throws IOException, Exception {
        Resource resource = packages.getResource(path);
        if (resource.exists()) {
            return Response
                    .ok()
                    .header("Content-Type", mimeTypes.getMimeByExtension(path))
                    .entity(resource.getInputStream())
                    .build();
        }
        else {
            Resource templateResource = packages.getResource(path + ".mustache");
            if (templateResource.exists()) {
                Template template = templates.get(templateResource);
                return Response
                        .ok()
                        .header("Content-Type", mimeTypes.getMimeByExtension(path))
                        .entity(template.execute(getWorkspace(name)))
                        .build();
            }
            else {
                return fallback.call();
            }
        }
    }

}